<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pizza extends Model
{
    protected $fillable = ['pizza_name', 'description', 'price', 'image'];

    public function pizza(){
        $this->belongsTo('App\Orders');
    }
}
