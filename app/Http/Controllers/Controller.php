<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function responser($item,$data,$name)
    {

        $num = $item->count();

        if($num > 0){
            return response()->json([
                'data' => $data,
                'status' => 200,
                'message' => $num.' '.$name.' found'
            ], 200);
        } else {
            return response()->json([
                'data' => $data,
                'status' => 404,
                'message' => $name.' not found'
            ], 404);
        }
    }

}
