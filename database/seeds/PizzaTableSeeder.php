<?php

use App\Pizza;
use App\State;
use Illuminate\Database\Seeder;

class PizzaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pizza::truncate();
        $faker = \Faker\Factory::create();

        for($i = 0; $i < 9; $i++){
            Pizza::create([
                'pizza_name' => $faker->sentence,
                'description' => $faker->paragraph,
                'image' => $faker->ImageUrl('https://upload.wikimedia.org/wikipedia/commons/d/d3/Supreme_pizza.jpg'),
            ]);
        }
    }
}
