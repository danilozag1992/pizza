<?php

namespace App\Http\Controllers;

use App\Http\Resources\Orders as OrderResource;
use App\Orders;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrdersController extends Controller
{
    public function index()
    {
        return Orders::all();
    }

    public function create(Request $request)
    {
        $order = Orders::create([
            'id' => $request->id,
            'user_id' => $request->user_id,
            'pizza_id' => $request->pizza_id,
            'quantity' => $request->quantity,
            'address' => $request->address,
            'phone' => $request->phone,
            'price' => $request->price,
            'total' => $request->total,
        ]);
        return response()->json($order, 201);
    }

    public function delete(Orders $order)
    {
        $order->delete();

        return response()->json(null, 204);
    }

}


