<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class Orders extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'pizza_id'=>$this->pizza_id,
            'user_id'=>$this->user_id,
            'total' => $this->total,
            'quantity' => $this->quantity,
            'address' => $this->address,
            'phone' => $this->phone,
            'sent' => $this->sent,
        ];
    }
}
