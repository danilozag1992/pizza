<?php
use App\Pizza;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/pizzas/{pizza}', 'PizzaController@show');
Route::post('/pizzas', 'PizzaController@store');
Route::get('/pizzas', 'PizzaController@index');
Route::put('/pizzas/{pizza}', 'PizzaController@update');
Route::delete('/pizzas/{pizza}', 'PizzaController@delete');
Route::post('/login', 'LoginController@login');
Route::post('/register', 'RegisterController@register');
Route::post('/logout', 'LoginController@logout')->middleware('auth:api');
//Ordersw
Route::post('/orders', 'OrdersController@create');
Route::delete('/orders/{order}', 'OrdersController@delete');
Route::get('/orders', 'OrdersController@index');
