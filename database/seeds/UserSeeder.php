<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $faker = \Faker\Factory::create();

        $password = Hash::make('password');

        User::create([
            'name' => 'Danilo',
            'email' => 'danilo@test.com',
            'password' => $password,
        ]);
        User::create([
            'name' => 'Guest',
            'email' => 'guest@gmail.com',
            'password' => 'guest',
        ]);

        for($i = 0; $i < 10; $i++){
            User::create([
                'name' => 'kapricoza',
                'email' => $faker->email,
                'password' => $password,
            ]);
        }

    }
}
