<?php

use App\Orders;
use App\User;
use Illuminate\Database\Seeder;

class OrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Orders::truncate();
        $faker = \Faker\Factory::create();
        $userIds = User::all()->pluck('id')->toArray();
        for($i = 0; $i < 9; $i++){
            Orders::create([
                'total' => $faker->randomFloat(),
                'address' => $faker->sentence,
                'phone' => $faker->sentence,
            ]);
        }
    }
}
