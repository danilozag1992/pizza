<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $fillable = ['pizza_id','user_id','total','quantity','address','phone','sent'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function pizza()
    {
        return $this->hasMany('App\Pizza');
    }
}
